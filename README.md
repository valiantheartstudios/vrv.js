# VRV.js
VRV.js was created by stampylongr (me) and Sorurus with the purpose of being a simple video downloader for [VRV](https://vrv.co). Some planned features are:

- Conversions of ```.m3u8``` files to ```.mp4``` videos.
- Integration with [FFmpeg](https://ffmpeg.org).
- Ability to add VRV username and password to syntax.
- A more user-friendly client.
That's it for now!

